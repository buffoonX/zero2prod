use rocket::async_test;
use rocket::http::{Header, Status};
use rocket::local::asynchronous::Client as AsyncClient;
use rocket::State;
use sqlx::postgres::PgPoolOptions;
use sqlx::{Pool, Postgres};

use lzl_lib::configuration::{get_config, Settings};
use lzl_lib::{main_app, DataConfig};

#[async_test]
async fn test1() {
    let client = async_client().await;
    let response = client.get("/api/v1/health_check").dispatch().await;
    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.into_bytes().await.unwrap().len(), 2);
}

#[async_test]
async fn subscribe_returns_200_for_valid_form() {
    let client = async_client().await;
    let body = "name=lzl&email=zlin25%40ur.rochester.edu";
    let response = client
        .post(v1("/subscriptions"))
        .header(Header::new(
            "Content-Type",
            "application/x-www-form-urlencoded",
        ))
        .body(body)
        .dispatch()
        .await;

    assert_eq!(response.status(), Status::Created);

    let rocket = State::get(client.rocket()).expect("fail to get rocket instance");
    let pool = handler(rocket);
    let saved = sqlx::query!("SELECT email, name FROM subscriptions",)
        .fetch_one(pool)
        .await
        .expect("fail to fetch saved subscriptions");

    assert_eq!(saved.name, "lzl");
    assert_eq!(saved.email, "zlin25@ur.rochester.edu");
    // remove the just created data
    sqlx::query!(
        r#"DELETE FROM subscriptions WHERE name = $1 AND email = $2;"#,
        saved.name,
        saved.email,
    )
    .execute(pool)
    .await
    .expect("fail to clean up test");
}

#[async_test]
async fn subscribe_returns_400_for_invalid_form() {
    let cases = vec![
        "name",
        "name=lzl&",
        "name=lzl",
        "email",
        "email=zr",
        "email=zr%40ur.com",
        "name&email",
    ];

    let client = async_client().await;
    for case in cases {
        let response = client
            .post(v1("/subscriptions"))
            .header(Header::new(
                "Content-Type",
                "application/x-www-form-urlencoded",
            ))
            .body(case)
            .dispatch()
            .await;

        assert_eq!(
            response.status(),
            Status::UnprocessableEntity,
            "The API did not return 400 for invalid form {}!",
            case
        );
    }
}

fn test_config() -> Settings {
    get_config().expect("fail to load config")
}

async fn async_client() -> AsyncClient {
    let db_uri = test_config().database.connection_string();
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&db_uri)
        .await
        .expect("fail to pool");

    AsyncClient::tracked(main_app(DataConfig { pool }))
        .await
        .expect("valid async rocket instance")
}

fn v1(uri: &str) -> String {
    format!("/api/v1/{}", uri)
}

fn handler(state: &State<DataConfig>) -> &Pool<Postgres> {
    &state.pool
}
