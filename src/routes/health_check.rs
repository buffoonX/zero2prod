use rocket::http::Status;
use rocket::response::status::Custom;

#[get("/health_check")]
pub async fn check() -> Custom<&'static str> {
    Custom(Status::Ok, "ok")
}
