use rocket::form::Form;
use rocket::http::Status;
use rocket::response::status::Custom;
use rocket::State;
use sqlx::types::chrono::Utc;
use sqlx::types::Uuid;

use super::super::DataConfig;

#[derive(FromForm)]
pub struct SubscribeForm {
    name: String,
    email: String,
}

#[post("/subscriptions", data = "<form>")]
pub async fn create_subscription(
    form: Form<SubscribeForm>,
    db_conn: &State<DataConfig>,
) -> Custom<String> {
    if form.name.is_empty() || form.email.is_empty() {
        Custom(
            Status::UnprocessableEntity,
            "both name and email must be non-empty".to_string(),
        )
    } else {
        match sqlx::query!(
            r#"insert into subscriptions (id, name, email, create_time) values ($1, $2, $3, $4);"#,
            Uuid::new_v4(),
            form.name,
            form.email,
            Utc::now(),
        )
        .execute(&db_conn.pool)
        .await
        {
            Ok(_) => Custom(Status::Created, format!("{}, {}", form.name, form.email)),
            Err(_) => Custom(
                Status::InternalServerError,
                format!("fail {}, {}", form.name, form.email),
            ),
        }
    }
}
