use lzl_lib::configuration::get_config;
use lzl_lib::{main_app, DataConfig};

use sqlx::postgres::PgPoolOptions;

#[macro_use]
extern crate rocket;

#[launch]
async fn rocket() -> _ {
    let configuration = get_config().expect("failed to load configuration");

    // load db config
    let db_uri = &configuration.database.connection_string();
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(db_uri)
        .await
        .expect("fail to pool");

    main_app(DataConfig { pool })
}
