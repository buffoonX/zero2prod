#[macro_use]
extern crate rocket;

pub mod configuration;
pub mod routes;

use routes::check;
use routes::create_subscription;
use sqlx::{Pool, Postgres};

pub struct DataConfig {
    pub pool: Pool<Postgres>,
}

pub fn main_app(db_config: DataConfig) -> rocket::Rocket<rocket::Build> {
    rocket::build()
        .mount("/api/v1", routes![check])
        .mount("/api/v1", routes![create_subscription])
        .manage(db_config)
}
